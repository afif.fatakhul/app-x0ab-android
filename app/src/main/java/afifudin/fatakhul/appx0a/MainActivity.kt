 package afifudin.fatakhul.appx0a

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

 class MainActivity : AppCompatActivity(), View.OnClickListener {


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnMhs ->{
                var mhs = Intent(this, MahasiswaActivity::class.java)
                startActivity(mhs)
                true
            }

            R.id.btnProdi ->{
                var prd = Intent(this, ProdiActivity::class.java)
                startActivity(prd)
                true
            }
        }
    }

     override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState)
         setContentView(R.layout.activity_main)
         btnMhs.setOnClickListener(this)
         btnProdi.setOnClickListener(this)
    }


}
